import {Component,Input,OnDestroy,OnInit,Output,EventEmitter,} from "@angular/core";
import { Post } from "../../models/post/post";
import { EditPost } from "../../models/post/edit-post";
import { AuthenticationService } from "../../services/auth.service";
import { ImgurService } from "../../services/imgur.service";
import { AuthDialogService } from "../../services/auth-dialog.service";
import { empty, Observable, Subject } from "rxjs";
import { DialogType } from "../../models/common/auth-dialog-type";
import { LikeService } from "../../services/like.service";
import { NewComment } from "../../models/comment/new-comment";
import { CommentService } from "../../services/comment.service";
import { User } from "../../models/user";
import { Comment } from "../../models/comment/comment";
import { catchError, switchMap, takeUntil } from "rxjs/operators";
import { SnackBarService } from "../../services/snack-bar.service";
import { PostService } from "../../services/post.service";
import { HubConnectionBuilder, HubConnection } from "@aspnet/signalr";
import { Reaction } from "src/app/models/reactions/reaction";
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ShareDialogComponent } from '../share-dialog/share-dialog.component';
import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: "app-post",
  templateUrl: "./post.component.html",
  styleUrls: ["./post.component.sass"],
})
export class PostComponent implements OnInit, OnDestroy {
  @Input() public post: Post;
  @Input() public currentUser: User;
  @Output() deletePostRequest = new EventEmitter<Post>();

  public imageUrl: string;
  public oldBody: string;

  public isEditPost = false;
  public isSharePost = false;
  public loading = false;

  public imageFile: File;

  public showComments = false;
  public newComment = {} as NewComment;
  //public reaction = {} as Reaction;

  private unsubscribe$ = new Subject<void>();

  public postHub: HubConnection;
  public constructor(public router:Router,        
    private location: Location,
    private authService: AuthenticationService,
    private authDialogService: AuthDialogService,
    private likeService: LikeService,
    private commentService: CommentService,
    private snackBarService: SnackBarService,
    private postService: PostService,
    private imgurService: ImgurService,
    private dialog: MatDialog
  ) {}

  public ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    this.postHub.stop();
  }

  public ngOnInit() {
    this.postService
    .getPost()
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe();
    this.registerHub();
  }
  
  public isCurrentAuthor() {
    if (this.currentUser == null) return false;
    else return this.post.author.id === this.currentUser.id;
  }
  public registerHub() {
    this.postHub = new HubConnectionBuilder()
      .withUrl("https://localhost:44344/notifications/post")
      .build();
    this.postHub
      .start()
      .catch((error) => this.snackBarService.showErrorMessage(error));

    this.postHub.on("NewReaction", (newReaction: Reaction) => {
      this.addNewReaction(newReaction);
    });
  }
  public toggleComments() {
    if (!this.currentUser) {
      this.catchErrorWrapper(this.authService.getUser())
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((user) => {
          if (user) {
            this.currentUser = user;
            this.showComments = !this.showComments;
          }
        });
      return;
    }

    this.showComments = !this.showComments;
  }
  public sharePost() {
    this.isSharePost = true;
    let dialogRef = this.dialog.open(ShareDialogComponent, {
                  height: '400px',
                  width: '300px',
                  data: {
                      postId: this.post.id,
                      senderName: this.currentUser.userName
                 }
              });
          
  }

  public deleteComment(deletedComment: Comment) {
    const deleteSubscroption = this.commentService.deleteComment(
      deletedComment
    );
    deleteSubscroption.pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
      let commentId = this.post.comments.findIndex(
        (comment) => comment.id == deletedComment.id
      );
      this.post.comments.splice(commentId, 1);
    });
    (error) => this.snackBarService.showErrorMessage(error);
  }
  public likePost(isLike: boolean) {
    if (!this.currentUser) {
      this.catchErrorWrapper(this.authService.getUser())
        .pipe(
          switchMap((userResp) =>
            this.likeService.likePost(this.post, userResp, isLike)
          ),
          takeUntil(this.unsubscribe$)
        )
        .subscribe((post) => (this.post = post));
      //this.addNewReaction(this.);

      return;
    }
    this.likeService
      .likePost(this.post, this.currentUser, isLike)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((post) => (this.post = post));
    // this.reaction.isLike = isLike;
    //this.reaction.user = this.currentUser;
    let react = {} as Reaction;
    react.isLike = isLike;
    react.user = this.currentUser;
    this.addNewReaction(react);
  }
  public addNewReaction(reaction: Reaction) {
    if (
      !this.post.reactions.some(
        (x) => x.user.id === reaction.user.id && x.isLike == reaction.isLike
      )
    ) {
      this.post.reactions = this.post.reactions.concat(reaction);
    }
  }
  public userList() {
    return this.post.reactions.forEach((element) => {
      element.user;
    });
  }

  public sendComment() {
    this.newComment.authorId = this.currentUser.id;
    this.newComment.postId = this.post.id;

    this.commentService
      .createComment(this.newComment)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          if (resp) {
            this.post.comments = this.sortCommentArray(
              this.post.comments.concat(resp.body)
            );
            this.newComment.body = undefined;
          }
        },
        (error) => this.snackBarService.showErrorMessage(error)
      );
  }
  editPost() {
    this.isEditPost = true;
    this.imageUrl = this.post.previewImage;
    this.oldBody = this.post.body;
  }
  public async saveEditPost() {
    if (
      this.oldBody == this.post.body &&
      this.imageUrl == this.post.previewImage
    ) {
      return;
    }
    let editPost = {} as EditPost;
    //this.setEditPost(editPost);

    let postSubscription;
    this.loading = true;
    if (this.imageUrl == this.post.previewImage) {
      this.setEditPost(editPost);
      postSubscription = this.postService.updatePost(editPost);
    } else if (!this.imageFile) {
      editPost.previewImage = undefined;
      this.setEditPost(editPost);
      postSubscription = this.postService.updatePost(editPost);
    } else {
      postSubscription = this.imgurService
        .uploadToImgur(this.imageFile, "title")
        .pipe(
          switchMap((imageData) => {
            this.post.previewImage = imageData.body.data.link;
            this.setEditPost(editPost);
            return this.postService.updatePost(editPost);
          })
        );
    }
    postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
      () => {
        this.removeImage();
        this.isEditPost = false;
        this.loading = false;
      },
      (error) => this.snackBarService.showErrorMessage(error)
    );
  }

  public loadImage(target: any) {
    this.imageFile = target.files[0];

    if (!this.imageFile) {
      target.value = "";
      return;
    }

    if (this.imageFile.size / 1000000 > 5) {
      target.value = "";
      this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
      return;
    }

    const reader = new FileReader();
    reader.addEventListener(
      "load",
      () => (this.imageUrl = reader.result as string)
    );
    reader.readAsDataURL(this.imageFile);
  }

  public setEditPost(editPost: EditPost) {
    editPost.id = this.post.id;
    editPost.body = this.post.body;
    editPost.previewImage = this.post.previewImage;
  }

  public removeImage() {
    this.imageUrl = undefined;
    this.imageFile = undefined;
  }

  public deletePost() {
    this.deletePostRequest.emit(this.post);
  }
  public openAuthDialog() {
    this.authDialogService.openAuthDialog(DialogType.SignIn);
  }

  private catchErrorWrapper(obs: Observable<User>) {
    return obs.pipe(
      catchError(() => {
        this.openAuthDialog();
        return empty();
      })
    );
  }

  private sortCommentArray(array: Comment[]): Comment[] {
    return array.sort(
      (a, b) => +new Date(b.createdAt) - +new Date(a.createdAt)
    );
  }

  public likeList() {
    return this.post.reactions.filter((x) => x.isLike == true);
  }
  public dislikeList() {
    return this.post.reactions.filter((x) => x.isLike == false);
  }
}
