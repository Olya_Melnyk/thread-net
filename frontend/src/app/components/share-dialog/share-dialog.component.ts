import { Component, OnInit,Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PostService } from 'src/app/services/post.service';
import { SharePost } from 'src/app/models/post/share-post';
import { SnackBarService } from 'src/app/services/snack-bar.service';

@Component({
  selector: 'app-share-dialog',
  templateUrl: './share-dialog.component.html',
  styleUrls: ['./share-dialog.component.sass']
})
export class ShareDialogComponent implements OnInit {
  public postId: number;
     public fromName: string;
      public toName: string;
  
      public isEmail = false;
      constructor(
          @Inject(MAT_DIALOG_DATA) public data: any,
          private postService: PostService,
         private snackBarService: SnackBarService) {
          this.postId = data.postId;
          this.fromName = data.fromName;
      }
  

  ngOnInit(): void {
  }
  public sharePostEmail() {
            const postShareDto: SharePost = {
                id: this.postId,
                fromName: this.fromName,
                toName: this.toName
            };
    
            this.postService.sharePostEmail(postShareDto).subscribe(() => {
                this.snackBarService.showUsualMessage("Email has been sent!");
                this.isEmail = false;
            }, (error) => {
                this.snackBarService.showErrorMessage(error);
            });
      }
    
}
