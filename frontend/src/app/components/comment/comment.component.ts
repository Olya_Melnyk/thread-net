import { Component, Input, OnDestroy,Output,EventEmitter } from "@angular/core";
import { Comment } from "../../models/comment/comment";
import { User } from "../../models/user";
import { AuthenticationService } from "../../services/auth.service";
import { empty, Observable, Subject } from "rxjs";
import { catchError, switchMap, takeUntil } from "rxjs/operators";
import { DialogType } from "../../models/common/auth-dialog-type";
import { CommentService } from "../../services/comment.service";
import { LikeService } from "../../services/like.service";
import { SnackBarService } from "../../services/snack-bar.service";
import { AuthDialogService } from "../../services/auth-dialog.service";
import { CommentEdit } from "../../models/comment/comment-edit";
@Component({
  selector: "app-comment",
  templateUrl: "./comment.component.html",
  styleUrls: ["./comment.component.sass"],
})
export class CommentComponent {
  @Input() public comment: Comment;
  @Input() public currentUser: User;
  @Output() deleteCommentRequest = new EventEmitter<Comment>();

  public oldBody: string;

  private unsubscribe$ = new Subject<void>();

  public isEditComment = false;
  public loading = false;

  public constructor(
    private authService: AuthenticationService,
    private authDialogService: AuthDialogService,
    private likeService: LikeService,
    private commentService: CommentService,
    private snackBarService: SnackBarService
  ) {}

  public editComment() {
    this.isEditComment = true;
    this.oldBody = this.comment.body;
  }

  public async saveEditComment() {
    if (this.oldBody == this.comment.body) {
      return;
    }

    let editComment = {} as CommentEdit;
    this.setEditComment(editComment);
    this.loading = true;

    this.commentService
      .updateComment(editComment)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((error) => this.snackBarService.showErrorMessage(error));

    this.isEditComment = false;
    this.loading = false;
  }

  public setEditComment(editComment: CommentEdit) {
    editComment.id = this.comment.id;
    editComment.body = this.comment.body;
  }

  public likeComment(isLike: boolean) {
    if (!this.currentUser) {
      this.catchErrorWrapper(this.authService.getUser())
        .pipe(
          switchMap((userResp) =>
            this.likeService.likeComment(this.comment, userResp, isLike)
          ),
          takeUntil(this.unsubscribe$)
        )
        .subscribe((comment) => (this.comment = comment));

      return;
    }

    this.likeService
      .likeComment(this.comment, this.currentUser, isLike)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((comment) => (this.comment = comment));
  }

  public deleteComment() {
    this.deleteCommentRequest.emit(this.comment);
  }

  private catchErrorWrapper(obs: Observable<User>) {
    return obs.pipe(
      catchError(() => {
        this.openAuthDialog();
        return empty();
      })
    );
  }
  public openAuthDialog() {
    this.authDialogService.openAuthDialog(DialogType.SignIn);
  }
  public likeList() {
    return this.comment.reactions.filter((x) => x.isLike == true);
  }
  public dislikeList() {
    return this.comment.reactions.filter((x) => x.isLike == false);
  }
}
