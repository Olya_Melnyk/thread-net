import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  constructor(private activeRoute: ActivatedRoute) {
    activeRoute.queryParams
      .subscribe((params) => 
        {
          console.log(params)
    });
  }
  ngOnInit(): void {
  }

}
