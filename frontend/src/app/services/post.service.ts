import { Injectable } from "@angular/core";
import { HttpInternalService } from "./http-internal.service";
import { Post } from "../models/post/post";
import { NewReaction } from "../models/reactions/newReaction";
import { NewPost } from "../models/post/new-post";
import { EditPost } from "../models/post/edit-post";
import { EventService } from './event.service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { SharePost } from '../models/post/share-post';

@Injectable({ providedIn: "root" })
export class PostService {
  public routePrefix = "/api/posts";
  private post:Post;
  constructor(private httpService: HttpInternalService, private eventService:EventService) {}

  public getPosts() {
    return this.httpService.getFullRequest<Post[]>(`${this.routePrefix}`);
  }

  public sharePostEmail(postShare: SharePost) {
            return this.httpService.postFullRequest(`${this.routePrefix}/share/email`, postShare)
      }
  public getPost(){
    return of(this.post);/*
    : this.getPostFromToken().pipe(
          map((resp) => {
            console.log(resp);
             // this.post = resp.body;
             // this.eventService.postChanged(this.post);
              return this.post;
          })
      );*/
  }

  public getPostFromToken()
  {
    return this.httpService.getFullRequest<Post>(`${this.routePrefix}/fromToken`);

  }
  public createPost(post: NewPost) {
    return this.httpService.postFullRequest<Post>(`${this.routePrefix}`, post);
  }

  public likePost(reaction: NewReaction) {
    return this.httpService.postFullRequest<Post>(
      `${this.routePrefix}/reaction`,
      reaction
    );
  }
  public updatePost(post: EditPost) {
    return this.httpService.putFullRequest<EditPost>(
      `${this.routePrefix}`,
      post
    );
  }
  public deletePost(post: Post) {
    return this.httpService.deleteRequest<Post>(
      `${this.routePrefix}/${post.id}`,
      post.id
    );
  }
  public dislikePost(reaction: NewReaction) {
    return this.httpService.postFullRequest<Post>(
      `${this.routePrefix}/reaction`,
      reaction
    );
  }
}
