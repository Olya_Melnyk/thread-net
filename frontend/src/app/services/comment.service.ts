import { Injectable } from "@angular/core";
import { HttpInternalService } from "./http-internal.service";
import { NewComment } from "../models/comment/new-comment";
import { Comment } from "../models/comment/comment";
import { CommentEdit } from "../models/comment/comment-edit";
import { NewReaction } from "../models/reactions/newReaction";

@Injectable({ providedIn: "root" })
export class CommentService {
  public routePrefix = "/api/comments";

  constructor(private httpService: HttpInternalService) {}

  public createComment(post: NewComment) {
    return this.httpService.postFullRequest<Comment>(
      `${this.routePrefix}`,
      post
    );
  }

  public deleteComment(comment: Comment) {
    return this.httpService.deleteRequest<Comment>(
      `${this.routePrefix}/${comment.id}`,
      comment
    );
  }

  public updateComment(comment: CommentEdit) {
    return this.httpService.putFullRequest<CommentEdit>(
      `${this.routePrefix}`,
      comment
    );
  }

  public reactionComment(reaction: NewReaction) {
    return this.httpService.postFullRequest<Comment>(
      `${this.routePrefix}/reaction`,
      reaction
    );
  }
}
