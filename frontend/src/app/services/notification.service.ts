import { Injectable } from "@angular/core";
import { ToastrService } from "ngx-toastr";

@Injectable({
  providedIn: "root",
})
export class NotificationService {
  constructor(private toastr: ToastrService) {}
  showHTMLMessage(message, title) {
    this.toastr.success(message, title, {
      enableHtml: true,
    });
  }
}
