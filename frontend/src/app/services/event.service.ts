import { Injectable } from "@angular/core";

import { Subject } from "rxjs";
import { User } from "../models/user";
import { Post } from "../models/post/post";

// tslint:disable:member-ordering
@Injectable({ providedIn: "root" })
export class EventService {
  private onUserChanged = new Subject<User>();
  public userChangedEvent$ = this.onUserChanged.asObservable();
  private onPostChansed = new Subject<Post>();
  public userChanged(user: User) {
    this.onUserChanged.next(user);
  }
  public postChanged(post: Post) {
    this.onPostChansed.next(post);
  }
}
