import { Directive } from "@angular/core";
import { NG_VALIDATORS, AbstractControl, Validator } from "@angular/forms";
import { Key } from "readline";

@Directive({
  selector: "[appEmailvalidate]",
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: EmailvalidateDirective,
      multi: true,
    },
  ],
})
export class EmailvalidateDirective {
  constructor() {}
  public patternEmail = "^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,}$";
  public regex = new RegExp(this.patternEmail);
  validate(control: AbstractControl): { [key: string]: any } | null {
    return ((control: AbstractControl): { [key: string]: any } | null => {
      const isCorrect = this.regex.test(control.value);
      return isCorrect ? null : { patternEmail: { value: control.value } };
    })(control);
  }
}
