export interface CommentEdit {
  id: number;
  body: string;
}
