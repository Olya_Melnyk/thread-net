﻿using System;
using System.Collections.Generic;
using System.Text;
using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.Common.DTO.Post
{
    public class ShareDTO
    {
        public int Id { get; set; }
        public string fromName { get; set; }
        public string toName { get; set; }
    }
}
