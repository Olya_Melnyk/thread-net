﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thread_.NET.Common.DTO.Post
{
    public class PostEditDTO
    {
        public int Id { get; set; }
        public string PreviewImage { get; set; }
        public string Body { get; set; }

    }
}
