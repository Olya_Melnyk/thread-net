﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thread_.NET.Common
{
    public class EmailMarkUp
    {
        public static string GetSharedPostMessage(string toName, string fromName, int postId)
        {
            string url = "http://localhost:4200/" + $"#{postId}";
            return
                 "<table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:600px; font-family: Verdana\">" +
                "<tbody style=\"background-color: #F2F8FD;\">" +
                "<tr style=\"height: 120px; background-color: #607d8b;\">" +
                "<td style=\"width: 170px;\"><img src=\"https://i.imgur.com/jgm17fm.png\" style=\"height: 100px; margin: 10px 0px 0px 40px;\" /></td>" +
                "<td style=\"color:white; font-size: 30px;\">Thread.NET</td></tr></tbody></table>" +
                "<table align=\"center\" border=\"0\" cellpadding=\"10\" cellspacing=\"0\" style=\"width:600px; font-family: Verdana\">" +
                "<tbody style=\"background-color: #F2F8FD; text-align: center;\">" +
                "<tr><td style=\"font-size: 1.5em;\">New notifications!</td></tr>" +
                $"<tr><td>Hello, {toName}!</td></tr>" +
                $"<tr><td>{fromName} shared a post with you!</td></tr>" +
                $"<tr><td><a href=\"{url}\">See the post</a></td></tr>" +
                $"<tr><td><br /></td></tr>" +
                $"</tbody></table>";
        }
    }
}
