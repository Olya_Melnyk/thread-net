﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Extensions;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly PostService _postService;
        private readonly LikeService _likeService;
        private readonly EmailService _emailService;
        public PostsController(PostService postService, LikeService likeService)//, EmailService emailService)
        {
            _postService = postService;
            _likeService = likeService;
            //_emailService = emailService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<ICollection<PostDTO>>> Get()
        {
            return Ok(await _postService.GetAllPosts());
        }

        
        [HttpDelete("{id}")]
        public async Task<ActionResult<PostDTO>> DeletePost(int id)
        {
            await _postService.deletePost(id);
            return NoContent();
        }
        [HttpPost("share/email")]
        public async Task<IActionResult> SharePostEmail([FromBody] ShareDTO postShare)
        {
            await _emailService.SharePost(postShare);
            return NoContent();
        }
        [HttpPost]
        public async Task<ActionResult<PostDTO>> CreatePost([FromBody] PostCreateDTO dto)
        {
            dto.AuthorId = this.GetUserIdFromToken();

            return Ok(await _postService.CreatePost(dto));
        }

        [HttpPost("reaction")]
        public async Task<IActionResult> LikePost(NewReactionDTO reaction)
        {
            reaction.UserId = this.GetUserIdFromToken();
            await _likeService.LikePost(reaction);
            return Ok();
        }

        [HttpPut]
        public async Task<ActionResult<PostEditDTO>> UpdatePost([FromBody] PostEditDTO dto)
        {
            await _postService.UpdatePost(dto);
            return NoContent();
        }

        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<PostDTO>> GetPostById(int id)
        {
            return Ok(await _postService.GetPostById(id));
        }

    }
}