﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class PostService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;

        public PostService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper)
        {
            _postHub = postHub;
        }

        public async Task<ICollection<PostDTO>> GetAllPosts()
        {
            var comments = await _context.Comments
               .Include(comment => comment.Reactions)
               .Include(comment => comment.Author)
               .Where(c => c.IsDeleted == false)
               .ToListAsync();

            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => comments)
                .Where(post=>post.IsDeleted==false)
                .OrderByDescending(post => post.CreatedAt)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<ICollection<PostDTO>> GetAllPosts(int userId)
        {
            var comments = await _context.Comments
               .Include(comment => comment.Reactions)
               .Include(comment => comment.Author)
               .Where(c => c.IsDeleted == false).ToListAsync();

            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => comments)
                .Where(p => p.AuthorId == userId && p.IsDeleted==false) // Filter here
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<PostDTO> CreatePost(PostCreateDTO postDto)
        {
            var postEntity = _mapper.Map<Post>(postDto);

            _context.Posts.Add(postEntity);
            await _context.SaveChangesAsync();

            var createdPost = await _context.Posts
                .Include(post => post.Author)
					.ThenInclude(author => author.Avatar)
                .FirstAsync(post => post.Id == postEntity.Id);

            var createdPostDTO = _mapper.Map<PostDTO>(createdPost);
            await _postHub.Clients.All.SendAsync("NewPost", createdPostDTO);

            return createdPostDTO;
        }

        public async Task UpdatePost(PostEditDTO postDto)
        {
            var postEntity = await GetPostByIdInternal(postDto.Id);

            if (postEntity == null)
            {
                throw new NotFoundException(nameof(Post), postDto.Id);
            }

            if(string.IsNullOrEmpty(postDto.Body)&&string.IsNullOrEmpty(postDto.PreviewImage))
            {
                throw new ArgumentException("Post is empty");
            }

            if(postEntity.Body!=postDto.Body)
            {
                postEntity.Body = postDto.Body??string.Empty;
            }

            if(postEntity.Preview.URL!=postDto.PreviewImage)
            {
                postEntity.Preview.URL = postDto.PreviewImage??string.Empty;
            }

            var timeNow = DateTime.Now;
            postEntity.UpdatedAt = timeNow;

            _context.Posts.Update(postEntity);
            await _context.SaveChangesAsync();
        }

        public async Task<PostDTO> deletePost(int id)
        {
            var postEntity = await GetPostByIdInternal(id);

            if (postEntity == null)
            {
                throw new NotFoundException(nameof(Post), id);
            }

            postEntity.IsDeleted = true;
            _context.Posts.Update(postEntity);

            await _context.SaveChangesAsync();


            var deletedPost = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .FirstAsync(post => post.Id == postEntity.Id);

           var deletedPostDTO = _mapper.Map<PostDTO>(deletedPost);

            await _postHub.Clients.All.SendAsync("DeletePost",deletedPostDTO);

            return deletedPostDTO;
        }

        public async Task<PostDTO> GetPostById(int id)
        {
            var commentList = await _context.Comments
                .Include(comment => comment.Author)
                .Where(c => c.IsDeleted == false).ToListAsync();
            var post = await _context.Posts
              .Include(post => post.Author)
                   .ThenInclude(author => author.Avatar)
               .Include(post => post.Preview)
               .Include(post => post.Reactions)
                   .ThenInclude(reaction => reaction.User)
               .Include(post => commentList)
               .FirstOrDefaultAsync(post => post.Id ==id);
            var postDto = _mapper.Map<PostDTO>(post);
         return postDto;            
        }
        private async Task<Post> GetPostByIdInternal(int id)
        {
            return await _context.Posts.Include(x=>x.Preview)
                .FirstOrDefaultAsync(u => u.Id == id);
        }
    }
}
