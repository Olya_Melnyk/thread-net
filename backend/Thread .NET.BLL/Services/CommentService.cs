﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        public CommentService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            return _mapper.Map<CommentDTO>(createdComment);
        }

        public async Task UpdateComment(CommentEditDTO commentEdit)
        {
            var commentEntity = await GetPostByIdInternal(commentEdit.Id);

            if (commentEntity == null)
            {
                throw new NotFoundException(nameof(Comment), commentEdit.Id);
            }

            if (string.IsNullOrEmpty(commentEdit.Body))
            {
                throw new ArgumentException("Post is empty");
            }

            if (commentEntity.Body != commentEdit.Body)
            {
                commentEntity.Body = commentEdit.Body ?? string.Empty;
            }

            var timeNow = DateTime.Now;
            commentEntity.UpdatedAt = timeNow;

            _context.Comments.Update(commentEntity);
            await _context.SaveChangesAsync();
        }

        public async Task deleteComment(int id)
        {
            var commentEntity = await GetPostByIdInternal(id);
            if (commentEntity == null)
            {
                throw new NotFoundException(nameof(Comment), id);
            }

            var timeNow = DateTime.Now;

            commentEntity.IsDeleted = true;
            commentEntity.UpdatedAt = timeNow;

            _context.Comments.Update(commentEntity);
            await _context.SaveChangesAsync();
        }

        private async Task<Comment> GetPostByIdInternal(int id)
        {
            return await _context.Comments
                //.Include(u=>u.Body)
                .FirstOrDefaultAsync(u => u.Id == id);
        }
    }
}
