﻿using AutoMapper;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.DAL.Context;
using Thread_.NET.BLL.Hubs;
using Microsoft.AspNetCore.SignalR;



namespace Thread_.NET.BLL.Services
{
    public sealed class LikeService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;

        public LikeService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper)
        {
            _postHub = postHub;
        }

        public async Task LikePost(NewReactionDTO reaction)
        {
             var likes = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId);
            ReactionDTO reactionDTO = new ReactionDTO();

            if (likes.Any())
            {
                _context.PostReactions.RemoveRange(likes);
            }
            else
            {
                var newReaction = new DAL.Entities.PostReaction
                {
                    PostId = reaction.EntityId,
                    IsLike = reaction.IsLike,
                    UserId = reaction.UserId
                };
                reactionDTO = _mapper.Map<ReactionDTO>(newReaction);

                _context.PostReactions.Add(newReaction);
            }
            await _context.SaveChangesAsync();

           // await _postHub.Clients.All.SendAsync("NewReaction", reactionDTO);
           return;
        }

        public async Task LikeComment(NewReactionDTO reaction)
        {
            var likes = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId);

            if (likes.Any())
            {
                _context.CommentReactions.RemoveRange(likes);
                await _context.SaveChangesAsync();

                return;
            }

            _context.CommentReactions.Add(new DAL.Entities.CommentReaction
            {
                CommentId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            });

            await _context.SaveChangesAsync();
        }
    }
}
