﻿﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Common;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public class EmailService:BaseService
    {
        private string emailFrom = Environment.GetEnvironmentVariable("THREADNET_EMAIL", EnvironmentVariableTarget.User);
        private string password = Environment.GetEnvironmentVariable("THREADNET_EMAIL_PASSWORD", EnvironmentVariableTarget.User);
        private ThreadContext _context;
        public EmailService(ThreadContext context,IMapper mapper):base(context,mapper)
        {
            _context = context;
        }
        public void Send(string emailTo, string subject, string body)
        {
            SmtpClient client = new SmtpClient("smtp.gmail.com");
            client.Port = 587;
            client.Credentials = new System.Net.NetworkCredential(emailFrom, password);
            client.EnableSsl = true;

            MailMessage message = new MailMessage(emailFrom,emailTo, subject, body);

            message.From = new MailAddress(emailFrom, "ThreadNet");
            message.IsBodyHtml = true;

            client.SendAsync(message, null);
        }

        public async Task SharePost(ShareDTO share)
        {
            if (!_context.Posts.Any(post => post.Id == share.Id))
            {
                throw new NotFoundException(nameof(Post), share.Id);
            }

            string email = _context.Users.FirstOrDefault(user => user.UserName == share.toName).Email;
            if (email == null)
            {
                throw new NotFoundException("Email");
            }

            string message = EmailMarkUp.GetSharedPostMessage(share.toName, share.fromName, share.Id);

            this.Send(email, $"{share.fromName} shared a post with you", message);
        }
            
    }    
}
