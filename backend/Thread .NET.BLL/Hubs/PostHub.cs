﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Common.DTO.Like;

namespace Thread_.NET.BLL.Hubs
{
    public sealed class PostHub : Hub
    {
        public async Task Send(PostDTO post)
        {
            await Clients.All.SendAsync("NewPost", post);
        }

        public async Task SendDelete(PostDTO post)
        {
            await Clients.All.SendAsync("DeletePost",post);
        }

        public async Task SendReaction(ReactionDTO reaction)
        {
            await Clients.All.SendAsync("NewReaction", reaction);
        }

    }
}
